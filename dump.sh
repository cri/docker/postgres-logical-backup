#! /usr/bin/env bash

# enable unofficial bash strict mode
set -o errexit
set -o nounset
set -o pipefail
IFS=$'\n\t'

ALL_DB_SIZE_QUERY="select sum(pg_database_size(datname)::numeric) from pg_database;"
PG_BIN=$PG_DIR/$PG_VERSION/bin
DUMP_SIZE_COEFF=5
ERRORCOUNT=0

TOKEN=$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)
K8S_API_URL=https://$KUBERNETES_SERVICE_HOST:$KUBERNETES_SERVICE_PORT/api/v1
CERT=/var/run/secrets/kubernetes.io/serviceaccount/ca.crt

function estimate_size {
    "$PG_BIN"/psql -tqAc "${ALL_DB_SIZE_QUERY}"
}

function dump {
    # settings are taken from the environment
    "$PG_BIN"/pg_dumpall
}

function compress {
    pigz
}

function aws_upload {
    declare -r EXPECTED_SIZE="$1"

    set +x # avoid leaking credentials
    [[ -f "$AWS_ACCESS_KEY_ID" ]] && AWS_ACCESS_KEY_ID="$(cat "$AWS_ACCESS_KEY_ID")"
    [[ -f "$AWS_SECRET_ACCESS_KEY" ]] && AWS_SECRET_ACCESS_KEY="$(cat "$AWS_SECRET_ACCESS_KEY")"
    set -x

    # mimic bucket setup from Spilo
    # to keep logical backups at the same path as WAL
    # NB: $LOGICAL_BACKUP_S3_BUCKET_SCOPE_SUFFIX already contains the leading "/" when set by the Postgres Operator
    PATH_TO_BACKUP=s3://$LOGICAL_BACKUP_S3_BUCKET"/spilo/"$SCOPE$LOGICAL_BACKUP_S3_BUCKET_SCOPE_SUFFIX"/logical_backups/"$(date +%s).sql.gz

    args=()

    [[ ! -z "$EXPECTED_SIZE" ]] && args+=("--expected-size=$EXPECTED_SIZE")
    [[ ! -z "$LOGICAL_BACKUP_S3_ENDPOINT" ]] && args+=("--endpoint-url=$LOGICAL_BACKUP_S3_ENDPOINT")
    [[ ! -z "$LOGICAL_BACKUP_S3_REGION" ]] && args+=("--region=$LOGICAL_BACKUP_S3_REGION")
    [[ ! -z "$LOGICAL_BACKUP_S3_SSE" ]] && args+=("--sse=$LOGICAL_BACKUP_S3_SSE")

    aws s3 cp - "$PATH_TO_BACKUP" "${args[@]//\'/}"
}

function gcs_upload {
    PATH_TO_BACKUP=gs://$LOGICAL_BACKUP_S3_BUCKET"/spilo/"$SCOPE$LOGICAL_BACKUP_S3_BUCKET_SCOPE_SUFFIX"/logical_backups/"$(date +%s).sql.gz

    gsutil -o Credentials:gs_service_key_file=$LOGICAL_BACKUP_GOOGLE_APPLICATION_CREDENTIALS cp - "$PATH_TO_BACKUP"
}

function restic_upload {
    # we mustn't put the date in this path as restic handles that for us
    # if we do, we won't be able to prune backups
    PATH_TO_BACKUP="spilo/"$SCOPE$LOGICAL_BACKUP_S3_BUCKET_SCOPE_SUFFIX"/logical_backups/$SCOPE.sql.gz"

    set +x # avoid leaking credentials
    # This is a hack as we can't easily pass extra environment variables.
    [[ -z "${RESTIC_PASSWORD:-}" ]] && RESTIC_PASSWORD="/etc/postgres-secrets/RESTIC_PASSWORD"

    [[ -f "$AWS_ACCESS_KEY_ID" ]] && export AWS_ACCESS_KEY_ID="$(cat "$AWS_ACCESS_KEY_ID")"
    [[ -f "$AWS_SECRET_ACCESS_KEY" ]] && export AWS_SECRET_ACCESS_KEY="$(cat "$AWS_SECRET_ACCESS_KEY")"
    [[ -f "$RESTIC_PASSWORD" ]] && export RESTIC_PASSWORD="$(cat "$RESTIC_PASSWORD")"
    set -x

    export RESTIC_REPOSITORY="s3:$LOGICAL_BACKUP_S3_ENDPOINT/$LOGICAL_BACKUP_S3_BUCKET"
    export HOSTNAME="spilo"
    export BACKUP_NAME="$SCOPE"

    # Init restic repo if needed
    # NB: `snapshots` cmd will fail not only if the repo is not initialized
    restic_wrong_pass="wrong password or no key found"
    restic_already_locked="unable to create lock in backend: repository is already locked exclusively"
    if ! restic_snapshot_output="$(restic snapshots 2>&1)"; then
        if grep -q "${restic_wrong_pass}" <<< "${restic_snapshot_output}"; then
            echo "Wrong restic repo password."
            exit 1
        elif grep -q "${restic_already_locked}" <<< "${restic_snapshot_output}"; then
            echo "Can't lock restic repo, repo is already locked"
            exit 2
        else
            echo "Restic repo is not initialized, initialize..."
            restic init
        fi
    fi

    restic backup --host "$HOSTNAME" --stdin --stdin-filename "$PATH_TO_BACKUP"
}

function upload {
    case $LOGICAL_BACKUP_PROVIDER in
        "aws")
            aws_upload $(($(estimate_size) / DUMP_SIZE_COEFF))
            ;;
        "gcs")
            gcs_upload
            ;;
        *)
            restic_upload
            ;;
    esac
}

function get_pods {
    declare -r SELECTOR="$1"

    curl "${K8S_API_URL}/namespaces/${POD_NAMESPACE}/pods?$SELECTOR" \
        --cacert $CERT \
        -H "Authorization: Bearer ${TOKEN}" | jq .items[].status.podIP -r
}

function get_current_pod {
    curl "${K8S_API_URL}/namespaces/${POD_NAMESPACE}/pods?fieldSelector=metadata.name%3D${HOSTNAME}" \
        --cacert $CERT \
        -H "Authorization: Bearer ${TOKEN}"
}

declare -a search_strategy=(
    list_all_replica_pods_current_node
    list_all_replica_pods_any_node
    get_master_pod
)

function list_all_replica_pods_current_node {
    get_pods "labelSelector=${CLUSTER_NAME_LABEL}%3D${SCOPE},spilo-role%3Dreplica&fieldSelector=spec.nodeName%3D${CURRENT_NODENAME}" | head -n 1
}

function list_all_replica_pods_any_node {
    get_pods "labelSelector=${CLUSTER_NAME_LABEL}%3D${SCOPE},spilo-role%3Dreplica" | head -n 1
}

function get_master_pod {
    get_pods "labelSelector=${CLUSTER_NAME_LABEL}%3D${SCOPE},spilo-role%3Dmaster" | head -n 1
}

CURRENT_NODENAME=$(get_current_pod | jq .items[].spec.nodeName --raw-output)
export CURRENT_NODENAME

for search in "${search_strategy[@]}"; do

    PGHOST=$(eval "$search")
    export PGHOST

    if [ -n "$PGHOST" ]; then
        break
    fi

done

set -x
dump | compress | upload
[[ ${PIPESTATUS[0]} != 0 || ${PIPESTATUS[1]} != 0 || ${PIPESTATUS[2]} != 0 ]] && (( ERRORCOUNT += 1 ))
set +x

exit $ERRORCOUNT
